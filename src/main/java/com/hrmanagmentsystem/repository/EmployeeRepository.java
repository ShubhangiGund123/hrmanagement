package com.hrmanagmentsystem.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.hrmanagmentsystem.bean.EmployeeDetails;

public interface EmployeeRepository extends CrudRepository<EmployeeDetails, String>{

	List findAll();
	
	EmployeeDetails findByEmpId(String empId);
}
