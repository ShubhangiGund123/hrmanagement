package com.hrmanagmentsystem.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.hrmanagmentsystem.bean.EmployeeDocument;

public interface EmployeeDocumentRepository extends CrudRepository<EmployeeDocument, String>{

	EmployeeDocument findByEmpIdAndDocTypeId(String empId, int docTypeId);
	
	List findByEmpId(String empId);
	
}
