package com.hrmanagmentsystem.repository;

import org.springframework.data.repository.CrudRepository;

import com.hrmanagmentsystem.bean.LoginDetails;
import java.lang.String;
import java.util.List;

public interface LoginDetailsRepository extends CrudRepository<LoginDetails, String>{

	LoginDetails findByUsernameAndPasswordAndStatus(String username, String password,int status);
	
	LoginDetails findByEmpId(String empId);
}
