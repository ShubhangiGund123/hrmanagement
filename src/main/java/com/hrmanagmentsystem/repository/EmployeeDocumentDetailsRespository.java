package com.hrmanagmentsystem.repository;

import org.springframework.data.repository.CrudRepository;

import com.hrmanagmentsystem.bean.EmployeeDocumentDetails;

public interface EmployeeDocumentDetailsRespository extends CrudRepository<EmployeeDocumentDetails, String>{

	EmployeeDocumentDetails findByDocTypeName(String docTypeName);
}
