package com.hrmanagmentsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrmanagmentsystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(HrmanagmentsystemApplication.class, args);
	}

}
