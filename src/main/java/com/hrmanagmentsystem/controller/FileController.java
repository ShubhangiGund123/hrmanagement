package com.hrmanagmentsystem.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.hrmanagmentsystem.serviceImpl.FileStorageServiceImpl;
import com.hrmanagmentsystem.services.FileStorageService;
import com.hrmanagmentsystem.bean.EmployeeDetails;
import com.hrmanagmentsystem.bean.UploadEmpDocument;
import com.hrmanagmentsystem.bean.UploadFileResponse;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("")
public class FileController {
	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	@Autowired
	private FileStorageService fileStorageService;

	@RequestMapping(value="/uploadFile", method=RequestMethod.POST)
	public UploadFileResponse uploadFile(@RequestBody UploadEmpDocument UploadEmpDocument) {
		String fileName = fileStorageService.storeFile(UploadEmpDocument);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
				.path(fileName).toUriString();

		return new UploadFileResponse(fileName, fileDownloadUri, UploadEmpDocument.getFile().getContentType(), UploadEmpDocument.getFile().getSize());
	}

	@RequestMapping(value="/uploadMultipleFiles", method=RequestMethod.POST)
	public List<UploadFileResponse> uploadMultipleFiles(@RequestBody List<UploadEmpDocument> UploadEmpDocumentList) {

		List list=new ArrayList();
		for(UploadEmpDocument UploadEmpDocument : UploadEmpDocumentList)
		{
			list.add(uploadFile(UploadEmpDocument));
		}

		return list;
	}

	@RequestMapping(value="/downloadFile", method=RequestMethod.POST)
	public List<ResponseEntity<Resource>> downloadFile(@RequestBody UploadEmpDocument uploadEmpDocument, HttpServletRequest request) {

		List list=new ArrayList();		
		for(int i=1;i<=5;i++)
		{
			// Load file as Resource
			Resource resource = fileStorageService.loadFileAsResource(uploadEmpDocument.getEmpid(), i);

			// Try to determine file's content type
			String contentType = null;
			try {
				contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
			} catch (IOException ex) {
				logger.info("Could not determine file type.");
			}

			// Fallback to the default content type if type could not be determined
			if (contentType == null) {
				contentType = "application/octet-stream";
			}

			list.add(ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
					.body(resource));
		}

		return list;
	}

}
