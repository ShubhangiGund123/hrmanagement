package com.hrmanagmentsystem.controller;

import java.util.HashMap;
import java.util.Map;

import javax.xml.ws.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hrmanagmentsystem.bean.LoginDetails;
import com.hrmanagmentsystem.constant.ResponseStatus;
import com.hrmanagmentsystem.services.LoginService;

@CrossOrigin(origins = "*", allowedHeaders ="*")
@RestController
@RequestMapping("")
public class LoginController {


	@Autowired
	LoginService loginService;

	String location="login controller";
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> Login(@RequestBody LoginDetails loginDetails)
	{

		final String success_msg="Login succesfull.";
		final String failure_msg="Fail to Login.";
		try {
			LoginDetails login =loginService.getLoginDetails(loginDetails);
			if(login!=null)
			{
				return BaseController.responseDataSuccess(login,success_msg, location);
			}else
			{
				return BaseController.responseDataFailure(failure_msg, location);
			}
		}catch (Exception e) {
			// TODO: handle exceptionResponseStatus.LOG.error("Inside ScenarioComparisonController - programScenarioListCalculatuon "+ e);
			ResponseStatus.LOG.error(" "+ e);
			return BaseController.responseDataFailure(failure_msg, location);

		}

	}

	@RequestMapping(value="/deleteUserAccess", method=RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> deleteUserAccess(@RequestBody LoginDetails loginDetails)
	{

		final String success_msg="delete User Access succesfull.";
		final String failure_msg="Fail to delete User Access.";
		try {
			int result =loginService.deleteUserAccess(loginDetails);
			if(result==1)
			{
				return BaseController.responseDataSuccess(1,success_msg, location);
			}else
			{
				return BaseController.responseDataFailure(failure_msg, location);
			}
		}catch (Exception e) {
			// TODO: handle exceptionResponseStatus.LOG.error("Inside ScenarioComparisonController - programScenarioListCalculatuon "+ e);
			ResponseStatus.LOG.error(" "+ e);
			return BaseController.responseDataFailure(failure_msg, location);

		}

	}
}
