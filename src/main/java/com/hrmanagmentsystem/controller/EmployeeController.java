package com.hrmanagmentsystem.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hrmanagmentsystem.bean.EmployeeDetails;
import com.hrmanagmentsystem.bean.LoginDetails;
import com.hrmanagmentsystem.constant.ResponseStatus;
import com.hrmanagmentsystem.services.EmployeeService;

@CrossOrigin(origins = "*", allowedHeaders ="*")
@RestController
@RequestMapping("")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	String location="Employee controller";

	@RequestMapping(value="/addEmployee", method=RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> addEmployee(@RequestBody EmployeeDetails EmployeeDetails)
	{

		final String success_msg="Employee Add successfully.";
		final String failure_msg="Fail to Add employee.";
		try {
			LoginDetails login=employeeService.addEmployee(EmployeeDetails);
			if(login!=null)
			{
				return BaseController.responseDataSuccess(login,success_msg, location);
			}else
			{
				return BaseController.responseDataFailure(failure_msg, location);
			}
		}catch (Exception e) {
			// TODO: handle exceptionResponseStatus.LOG.error("Inside ScenarioComparisonController - programScenarioListCalculatuon "+ e);
			ResponseStatus.LOG.error(" "+ e);
			return BaseController.responseDataFailure(failure_msg, location);

		}

	}

	@RequestMapping(value="/getEmployeeList", method=RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> getEmployeeList()
	{

		final String success_msg="Employee List get successfully.";
		final String failure_msg="Fail to get employee list.";
		final String failure_msg1="employee list not available.";
		try {
			List<EmployeeDetails> EmployeeList=employeeService.getEmployeeList();
			if(EmployeeList!=null)
			{
				return BaseController.responseDataSuccess(EmployeeList,success_msg, location);
			}else
			{
				return BaseController.responseDataFailure(failure_msg, location);
			}
		}catch (Exception e) {
			// TODO: handle exceptionResponseStatus.LOG.error("Inside ScenarioComparisonController - programScenarioListCalculatuon "+ e);
			ResponseStatus.LOG.error(" "+ e);
			return BaseController.responseDataFailure(failure_msg, location);

		}

	}


	@RequestMapping(value="/updateEmployee", method=RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> updateEmployee(@RequestBody EmployeeDetails EmployeeDetails)
	{

		final String success_msg="Employee Update successfully.";
		final String failure_msg="Fail to Update employee.";
		try {
			EmployeeDetails emp=employeeService.updateEmployee(EmployeeDetails);
			if(emp!=null)
			{
				return BaseController.responseDataSuccess(emp,success_msg, location);
			}else
			{
				return BaseController.responseDataFailure(failure_msg, location);
			}
		}catch (Exception e) {
			// TODO: handle exceptionResponseStatus.LOG.error("Inside ScenarioComparisonController - programScenarioListCalculatuon "+ e);
			ResponseStatus.LOG.error(" "+ e);
			return BaseController.responseDataFailure(failure_msg, location);

		}

	}

	@RequestMapping(value="/getEmployeeDocumentList", method=RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> getEmployeeDocumentList(@RequestBody EmployeeDetails EmployeeDetails)
	{

		final String success_msg="Get Employee Document successfully.";
		final String failure_msg="Fail to get employee Document.";
		try {
			Map list=employeeService.getEmployeeDocumentList(EmployeeDetails);
			return BaseController.responseDataSuccess(list,success_msg, location);
		}catch (Exception e) {
			// TODO: handle exceptionResponseStatus.LOG.error("Inside ScenarioComparisonController - programScenarioListCalculatuon "+ e);
			ResponseStatus.LOG.error(" "+ e);
			return BaseController.responseDataFailure(failure_msg, location);

		}

	}

}
