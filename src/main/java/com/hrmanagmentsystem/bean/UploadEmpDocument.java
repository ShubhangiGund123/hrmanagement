package com.hrmanagmentsystem.bean;

import org.springframework.web.multipart.MultipartFile;

public class UploadEmpDocument {

	private String empid;
	private int documentType; // 1- Adhar, 2- PAN card, 3-certificate, 4 -graduation_certificate, 5- other
	private MultipartFile file;
	
	public UploadEmpDocument() {}
	
	public String getEmpid() {
		return empid;
	}

	public void setEmpid(String empid) {
		this.empid = empid;
	}

	public int getDocumentType() {
		return documentType;
	}
	public void setDocumentType(int documentType) {
		this.documentType = documentType;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
	
}
