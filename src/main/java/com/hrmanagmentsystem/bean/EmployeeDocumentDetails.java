package com.hrmanagmentsystem.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EmployeeDocumentDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int docTypeId;
	private String docTypeName;
	private String updateDate;
	private String docOwner;
	public int getDocTypeId() {
		return docTypeId;
	}
	public void setDocTypeId(int docTypeId) {
		this.docTypeId = docTypeId;
	}
	public String getDocTypeName() {
		return docTypeName;
	}
	public void setDocTypeName(String docTypeName) {
		this.docTypeName = docTypeName;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getDocOwner() {
		return docOwner;
	}
	public void setDocOwner(String docOwner) {
		this.docOwner = docOwner;
	}
	
}
