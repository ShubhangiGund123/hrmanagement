package com.hrmanagmentsystem.constant;

import java.time.OffsetDateTime; 
import java.time.ZoneOffset;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hrmanagmentsystem.controller.LoginController;



public class ResponseStatus {
	public static final String SUCCESS="SUCCESS";
	public static final String FAILED="FAILURE";
	public static final String DATA_NOT_FOUND="DATA NOT FOUND";
	public static final String Auth_Success= "Authenticated successfully.";
	public static final String Invalid_Credentials= "Invalid credentials. Please check the username and  password.";
	public static final String Empty_Credentials= "Username or password cannot be empty.";
	public static final String LOGOUT_Success= "User Logout successfully.";
	public static final String EmailSent_SUCCESS= "Password reset successful.";
	public static final String FAIL="FAIL";
	public static final String PASS="PASS";
	public static final String EMAIL_ALREADY_EXISTS="Email Already Exists";
	public static final String Success_Code="200(ok)";
	public static final String Failed_Code="422(Bad Request)";
	public static final String Error_Code="500(Exception)";
	public static final String fileuplod= "file upload successfully.";
	public static final String empty_file= "Empty file is not allow.";
	public static final String fileUploadFailure ="File Upload Failure";
	public static final OffsetDateTime currentDate = OffsetDateTime.now(ZoneOffset.UTC );
	public static final String PICTURE_SAVE_SUCCESSFULLY= "Picture Save successfully.";
	public static final String FAIL_TO_SAVE_PICTURE ="Picture Save Failure";
	public static final String failtouploadfile="Fail to upload file";
	public static final Logger LOG = LogManager.getLogger(LoginController.class);
	
	
	
}
