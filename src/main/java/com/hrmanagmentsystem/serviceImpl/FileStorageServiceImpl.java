package com.hrmanagmentsystem.serviceImpl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.hrmanagmentsystem.bean.EmployeeDocument;
import com.hrmanagmentsystem.bean.EmployeeDocumentDetails;
import com.hrmanagmentsystem.bean.FileStorageProperties;
import com.hrmanagmentsystem.bean.UploadEmpDocument;
import com.hrmanagmentsystem.exception.FileStorageException;
import com.hrmanagmentsystem.exception.MyFileNotFoundException;
import com.hrmanagmentsystem.repository.EmployeeDocumentDetailsRespository;
import com.hrmanagmentsystem.repository.EmployeeDocumentRepository;
import com.hrmanagmentsystem.services.FileStorageService;

@Component
public class FileStorageServiceImpl implements FileStorageService{

	@Autowired
	EmployeeDocumentRepository employeeDocumentRepository;
	
	@Autowired
	EmployeeDocumentDetailsRespository employeeDocumentDetailsRespository;
	
	private final Path fileStorageLocation =Paths.get("C:\\D\\HACKATHON\\uploadefiles").toAbsolutePath().normalize();
	/*
	private final Path fileStorageLocation;

	@Autowired
	public FileStorageServiceImpl(FileStorageProperties fileStorageProperties) {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
					ex);
		}
	}
*/
	public String storeFile(UploadEmpDocument uploadEmpDocument) {
		// Normalize file name
		//		String fileName = uploadEmpDocument.getEmpid()+"_"+uploadEmpDocument.getDocumentType()+"_"+StringUtils.cleanPath(uploadEmpDocument.getFile().getOriginalFilename());
		//String fileName = uploadEmpDocument.getEmpid()+"_"+uploadEmpDocument.getDocumentType();

        String fileName = StringUtils.cleanPath(uploadEmpDocument.getFile().getOriginalFilename());
		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}

			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(uploadEmpDocument.getFile().getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			
			String docTypeName="";
			if(uploadEmpDocument.getDocumentType()==1)
			{
				docTypeName="Adhar Card";
			}
			else if(uploadEmpDocument.getDocumentType()==2)
			{
				docTypeName="Pan Card";
			}
			else if(uploadEmpDocument.getDocumentType()==3)
			{
				docTypeName="Certificate";
			}
			else if(uploadEmpDocument.getDocumentType()==4)
			{
				docTypeName="Graduation certificate";
			}
			else if(uploadEmpDocument.getDocumentType()==5)
			{
				docTypeName="Other";
			}
			EmployeeDocumentDetails employeeDocumentDetails=employeeDocumentDetailsRespository.findByDocTypeName(docTypeName);
		
			EmployeeDocument employeeDocument=employeeDocumentRepository.findByEmpIdAndDocTypeId(uploadEmpDocument.getEmpid(), employeeDocumentDetails.getDocTypeId());
			

			if(employeeDocument!=null)
			{
				employeeDocument.setDocName(fileName);
				employeeDocument.setUpdateDate(new Date().toString());
				employeeDocument.setDocOwner(employeeDocumentDetails.getDocOwner());
				employeeDocument.setDocument(uploadEmpDocument.getFile().getBytes());
				employeeDocumentRepository.save(employeeDocument);
			}
			else
			{
				EmployeeDocument employeeDocument1= new EmployeeDocument();
				employeeDocument1.setEmpId(uploadEmpDocument.getEmpid());
				employeeDocument1.setDocName(fileName);
				employeeDocument1.setUpdateDate(new Date().toString());
				employeeDocument1.setDocOwner(employeeDocumentDetails.getDocOwner());
				employeeDocumentRepository.save(employeeDocument1);
				
			}
			

			return fileName;
		} catch (IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	public Resource loadFileAsResource(String empId, int documentType) {
		String fileName=empId+"_"+documentType;
		try {
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new MyFileNotFoundException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			throw new MyFileNotFoundException("File not found " + fileName, ex);
		}
	}

}
