package com.hrmanagmentsystem.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.hrmanagmentsystem.bean.LoginDetails;
import com.hrmanagmentsystem.repository.LoginDetailsRepository;
import com.hrmanagmentsystem.services.LoginService;

@Component
public class LoginServiceImpl implements LoginService{

	@Autowired
	LoginDetailsRepository loginDetailsRepository;

	@Override
	public LoginDetails getLoginDetails(LoginDetails loginDetails) {
		// TODO Auto-generated method stub
		LoginDetails login=loginDetailsRepository.findByUsernameAndPasswordAndStatus(loginDetails.getUsername(), loginDetails.getPassword(),1);

		return login;
	}

	@Override
	public int deleteUserAccess(LoginDetails loginDetails) {
		// TODO Auto-generated method stub
		try {
			LoginDetails login=loginDetailsRepository.findByEmpId(loginDetails.getEmpId());
			login.setStatus(0);
			loginDetailsRepository.save(login);
			return 1;
			
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return 0;
		}
		
		
	}

}
