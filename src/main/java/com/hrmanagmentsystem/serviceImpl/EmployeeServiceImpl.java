package com.hrmanagmentsystem.serviceImpl;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;

import com.hrmanagmentsystem.bean.EmployeeDetails;
import com.hrmanagmentsystem.bean.EmployeeDocument;
import com.hrmanagmentsystem.bean.LoginDetails;
import com.hrmanagmentsystem.repository.EmployeeDocumentRepository;
import com.hrmanagmentsystem.repository.EmployeeRepository;
import com.hrmanagmentsystem.repository.LoginDetailsRepository;
import com.hrmanagmentsystem.services.EmployeeService;

@Component
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	EmployeeDocumentRepository employeeDocumentRepository;
	@Autowired
	LoginDetailsRepository loginDetailsRepository;

	@Override
	public LoginDetails addEmployee(EmployeeDetails employee) {
		// TODO Auto-generated method stub
		employee.setStatus(1);
		EmployeeDetails emp=employeeRepository.save(employee);

		LoginDetails loginDetails=new LoginDetails();
		loginDetails.setEmpId(emp.getEmpId());
		loginDetails.setUsername(emp.getFname().toLowerCase()+""+emp.getLname().toLowerCase());
		loginDetails.setPassword("123456");
		loginDetails.setRoll(employee.getRoll());
		loginDetails.setStatus(1);

		LoginDetails login=loginDetailsRepository.save(loginDetails);
		return login;
	}

	@Override
	public List<EmployeeDetails> getEmployeeList() {
		// TODO Auto-generated method stub
		List<EmployeeDetails> employeeList=employeeRepository.findAll();
		return employeeList;
	}

	@Override
	public EmployeeDetails updateEmployee(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		try {
			EmployeeDetails emp=employeeRepository.findByEmpId(employeeDetails.getEmpId());

			emp.setFname(employeeDetails.getFname());
			emp.setLname(employeeDetails.getLname());
			emp.setAddress(employeeDetails.getAddress());
			emp.setEmail_id(employeeDetails.getEmail_id());
			emp.setMobile_no(employeeDetails.getMobile_no());
			emp=employeeRepository.save(employeeDetails);
			return emp;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Map getEmployeeDocumentList(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		Map empDocumentDetails= new HashMap();
		Map list= new HashMap();
		try {
			System.out.println("employeeDetails  ="+employeeDetails.getEmpId());
			//		String urlPath	="C:\\D\\HACKATHON\\uploadefiles";
			List<EmployeeDocument> EmployeeDocumentList =employeeDocumentRepository.findByEmpId(employeeDetails.getEmpId());
			if(EmployeeDocumentList!=null)
			{

			}
			list.put("EmpDocList", EmployeeDocumentList);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return list;
	}

}
