package com.hrmanagmentsystem.services;

import org.springframework.core.io.Resource;

import com.hrmanagmentsystem.bean.UploadEmpDocument;

public interface FileStorageService {

	String storeFile(UploadEmpDocument uploadEmpDocument);

	Resource loadFileAsResource(String empId, int i);

}
