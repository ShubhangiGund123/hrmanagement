package com.hrmanagmentsystem.services;

import java.util.List;
import java.util.Map;

import com.hrmanagmentsystem.bean.EmployeeDetails;
import com.hrmanagmentsystem.bean.LoginDetails;

public interface EmployeeService {

	LoginDetails addEmployee(EmployeeDetails employee);

	List<EmployeeDetails> getEmployeeList();

	EmployeeDetails updateEmployee(EmployeeDetails employeeDetails);

	Map getEmployeeDocumentList(EmployeeDetails employeeDetails);
}
