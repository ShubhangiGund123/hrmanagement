package com.hrmanagmentsystem.services;

import com.hrmanagmentsystem.bean.LoginDetails;

public interface LoginService {

	LoginDetails getLoginDetails(LoginDetails loginDetails);

	int deleteUserAccess(LoginDetails loginDetails);

}
